"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.blackOutline = exports.black = exports.withCallback = exports.largeRoundedOutline = exports.pill = exports.rounded = exports.outline = exports.full = exports.large = exports.small = exports.basic = exports.default = void 0;

var _GambiApp = _interopRequireDefault(require("../../page/App/GambiApp"));

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@storybook/react");

var _addonInfo = require("@storybook/addon-info");

var _addonConsole = require("@storybook/addon-console");

var _Button = _interopRequireDefault(require("./Button"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _addonConsole.setConsoleOptions)({
  panelExclude: []
});
(0, _react2.addDecorator)(_addonInfo.withInfo); // Place outside of router

var _default = {
  title: 'Button',
  decorators: [_addonInfo.withInfo]
};
exports.default = _default;

var basic = function basic() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, null, "Click Me"));
};

exports.basic = basic;

var small = function small() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    small: true
  }, "Click Me"));
};

exports.small = small;

var large = function large() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    large: true
  }, "Click Me"));
};

exports.large = large;

var full = function full() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    full: true
  }, "Click Me"));
};

exports.full = full;

var outline = function outline() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    outline: true
  }, "Click Me"));
};

exports.outline = outline;

var rounded = function rounded() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    rounded: true
  }, "Click Me"));
};

exports.rounded = rounded;

var pill = function pill() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    pill: true
  }, "Click Me"));
};

exports.pill = pill;

var largeRoundedOutline = function largeRoundedOutline() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    large: true,
    rounded: true,
    outline: true
  }, "Click Me"));
};

exports.largeRoundedOutline = largeRoundedOutline;

var withCallback = function withCallback() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    onClick: function onClick() {
      console.log("Button Clicked");
    }
  }, "Click Me"), /*#__PURE__*/_react.default.createElement("p", null, "Open the console to view the output data"));
};

exports.withCallback = withCallback;

var black = function black() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    black: true
  }, "Click Me"));
};

exports.black = black;

var blackOutline = function blackOutline() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    black: true,
    outline: true
  }, "Click Me"));
};

exports.blackOutline = blackOutline;