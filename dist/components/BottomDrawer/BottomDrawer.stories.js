"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Drawer = exports.default = void 0;

var _GambiApp = _interopRequireDefault(require("../../page/App/GambiApp"));

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@storybook/react");

var _addonInfo = require("@storybook/addon-info");

var _addonConsole = require("@storybook/addon-console");

var _BottomDrawer = _interopRequireDefault(require("./BottomDrawer"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _addonConsole.setConsoleOptions)({
  panelExclude: []
});
(0, _react2.addDecorator)(_addonInfo.withInfo); // Place outside of router

var _default = {
  title: 'Drawer',
  decorators: [_addonInfo.withInfo]
};
exports.default = _default;

var Drawer = function Drawer() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_BottomDrawer.default, {
    open: true
  }, "Hello"));
};

exports.Drawer = Drawer;