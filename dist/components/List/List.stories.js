"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.callback = exports.multiSelectable = exports.selectable = exports.staticList = exports.default = void 0;

var _GambiApp = _interopRequireDefault(require("../../page/App/GambiApp"));

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@storybook/react");

var _addonInfo = require("@storybook/addon-info");

var _addonConsole = require("@storybook/addon-console");

var _List = _interopRequireDefault(require("./List"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _addonConsole.setConsoleOptions)({
  panelExclude: []
});
(0, _react2.addDecorator)(_addonInfo.withInfo); // Place outside of router

var _default = {
  title: 'List',
  decorators: [_addonInfo.withInfo]
};
exports.default = _default;

var staticList = function staticList() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_List.default, {
    options: ["São Paulo", "Rio de Janeiro", "Brasília", "Salvador", "Fortaleza"]
  }));
};

exports.staticList = staticList;

var selectable = function selectable() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_List.default, {
    options: ["São Paulo", "Rio de Janeiro", "Brasília", "Salvador", "Fortaleza"],
    selectable: true
  }));
};

exports.selectable = selectable;

var multiSelectable = function multiSelectable() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_List.default, {
    options: ["São Paulo", "Rio de Janeiro", "Brasília", "Salvador", "Fortaleza"],
    multi: true,
    selectable: true
  }));
};

exports.multiSelectable = multiSelectable;

var callback = function callback() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_List.default, {
    options: ["São Paulo", "Rio de Janeiro", "Brasília", "Salvador", "Fortaleza"],
    selectable: true,
    onUpdate: function onUpdate(val) {
      return console.log(val);
    }
  }));
};

exports.callback = callback;