"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

require("./Menu.css");

var _react2 = require("glamor/react");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// Based on https://codesandbox.io/s/github/wizardry-io/animated-modal/tree/1cdedfe4ad2ad728f4226ba896c5f3a5bb2fb850/?from-embed=&file=/src/App.js:42-88

/* @jsx createElement */
var redColor = "#ED5565"; //const mediumColor = "#ED5565";

var mediumColor = "#F66459";
var orangeColor = "#FC6E51";
var lightGrey = "#DCDAE1";
var black = "#333";
var yellow = "#E9B308";
var white = "#FFF";
var beige = "#EAB49E";
var transparentWhite = "rgba(255,255,255,.9)";

var Dot = function Dot() {
  return (0, _react2.createElement)("div", {
    css: {
      width: 2,
      height: 2,
      backgroundColor: lightGrey,
      borderRadius: 2,
      marginTop: 2
    }
  });
};

var MenuEntry = /*#__PURE__*/function (_Component) {
  _inherits(MenuEntry, _Component);

  var _super = _createSuper(MenuEntry);

  function MenuEntry(props) {
    var _this;

    _classCallCheck(this, MenuEntry);

    _this = _super.call(this, props);

    if (_this.props.transitionDuration) {
      _this.transitionDuration = _this.props.transitionDuration;
    } else {
      _this.transitionDuration = 1;
    }

    if (_this.props.callback != undefined) {
      _this.cb = _this.props.callback.bind(_assertThisInitialized(_this));
    } else {
      _this.cb = _this.emptyFn.bind(_assertThisInitialized(_this));
    }

    _this.transitionDelay = 0.25;
    return _this;
  }

  _createClass(MenuEntry, [{
    key: "emptyFn",
    value: function emptyFn() {
      console.warn("Callback not implemented for MenuEntry");
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return (0, _react2.createElement)("div", {
        css: {
          zIndex: 1,
          transitionProperty: "transform",
          transitionDuration: "".concat(this.transitionDuration, "s"),
          transitionDelay: this.transitionDelay,
          transform: "translateY(".concat(this.props.isOpen ? 0 : 20, "px)"),
          listStyleType: "none" //fontSize: "16pt"

        },
        key: this.props.menuItem["Title"]
      }, (0, _react2.createElement)("a", {
        css: {
          cursor: "pointer",
          opacity: this.props.isOpen ? 1 : 0,
          transitionProperty: "opacity",
          transitionDuration: "".concat(this.transitionDuration, "s"),
          transitionDelay: this.transitionDelay,
          zIndex: 1,
          color: white
        },
        className: "gd-menu-item",
        onClick: function onClick() {
          _this2.cb(_this2.props.menuItem["Loc"]);
        }
      }, this.props.menuItem["Title"]));
    }
  }]);

  return MenuEntry;
}(_react.Component);

var GambiMenu = /*#__PURE__*/function (_Component2) {
  _inherits(GambiMenu, _Component2);

  var _super2 = _createSuper(GambiMenu);

  function GambiMenu(props) {
    var _this3;

    _classCallCheck(this, GambiMenu);

    _this3 = _super2.call(this, props);
    _this3.transitionDuration = 1;

    if (_this3.props.transitionDuration) {
      _this3.transitionDuration = _this3.props.transitionDuration;
    }

    _this3.cb = _this3.props.callback.bind(_assertThisInitialized(_this3));
    return _this3;
  }

  _createClass(GambiMenu, [{
    key: "render",
    value: function render() {
      var _this4 = this;

      return (0, _react2.createElement)("div", {
        css: {
          position: "absolute",
          left: 0,
          top: 0,
          width: "100%",
          height: "100%",
          overflow: "hidden",
          pointerEvents: this.props.isOpen ? "initial" : "none",
          zIndex: 2
        }
      }, (0, _react2.createElement)("div", {
        css: {
          display: "grid",
          flexDirection: "column",
          paddingTop: 40,
          paddingLeft: 50,
          gridGap: 25
        }
      }, this.props.menuItems.map(function (menuItem, index) {
        return (0, _react2.createElement)(MenuEntry, {
          transitionDuration: _this4.transitionDuration,
          index: index,
          isOpen: _this4.props.isOpen,
          menuItem: menuItem,
          key: menuItem["Title"],
          callback: _this4.cb
        });
      })), (0, _react2.createElement)("div", {
        css: {
          position: "absolute",
          willChange: "transform",
          top: 0,
          left: 0,
          backgroundColor: transparentWhite,
          width: "220vh",
          height: "220vh",
          transform: "translate(-50%,-50%) ".concat(this.props.isOpen ? "scale(1)" : "scale(0)"),
          transitionProperty: "transform",
          transitionDuration: "".concat(this.transitionDuration / 2, "s"),
          transitionDelay: !this.props.isOpen && "".concat(this.transitionDuration / 2, "s"),
          borderRadius: "100%"
        }
      }), (0, _react2.createElement)("a", {
        id: "gd-menu-back",
        css: {
          width: "100vw",
          height: "100vh",
          position: "absolute",
          top: 0,
          left: 0,
          padding: 0,
          margin: 0,
          backgroundColor: transparentWhite,
          zIndex: 0,
          transform: "scale(".concat(this.props.isOpen ? "100%" : "0", ", ").concat(this.props.isOpen ? "100%" : "0", ")"),
          transitionProperty: "scale",
          transitionDuration: "".concat(this.transitionDuration, "s"),
          transitionTimingFunction: this.props.isOpen ? "cubic-bezier(0.5, 1, 0.3, 1.3)" : "cubic-bezier(0.5, -1, 0.3, 1.3)",
          cursor: "pointer"
        },
        onClick: this.props.onCloseButtonClick
      }), (0, _react2.createElement)("div", {
        css: {
          position: "absolute",
          willChange: "transform",
          top: "-20vh",
          marginTop: 45,
          left: 0,
          backgroundColor: mediumColor,
          width: "calc(140vh + 20vh * 2)",
          height: "calc(140vh + 20vh * 2)",
          transform: "translate(-50%,-50%) ".concat(this.props.isOpen ? "scale(1)" : "scale(0)"),
          transitionProperty: "transform",
          transitionDuration: "".concat(this.transitionDuration / 2, "s"),
          transitionDelay: !this.props.isOpen && "".concat(this.transitionDuration / 2, "s"),
          borderRadius: "100%"
        }
      }), (0, _react2.createElement)("div", {
        css: {
          position: "absolute",
          willChange: "transform",
          top: 40,
          left: 40,
          backgroundColor: redColor,
          width: "calc(70vh*2)",
          height: "calc(70vh*2)",
          transform: "translate(-50%,-50%) ".concat(this.props.isOpen ? "scale(1)" : "scale(0)"),
          transitionProperty: "transform",
          transitionDuration: "".concat(this.transitionDuration / 2, "s"),
          transitionDelay: !this.props.isOpen && "".concat(this.transitionDuration / 2, "s"),
          borderRadius: "100%"
        }
      }));
    }
  }]);

  return GambiMenu;
}(_react.Component);

var Menu = /*#__PURE__*/function (_Component3) {
  _inherits(Menu, _Component3);

  var _super3 = _createSuper(Menu);

  function Menu(props) {
    var _this5;

    _classCallCheck(this, Menu);

    _this5 = _super3.call(this, props);
    _this5.state = {
      isMenuOpened: false
    };
    _this5.items = _this5.props.entries;

    if (_this5.props.callback != undefined) {
      _this5.cb = _this5.props.callback.bind(_assertThisInitialized(_this5));
    } else {
      _this5.cb = _this5.emptyFn.bind(_assertThisInitialized(_this5));
    }

    return _this5;
  }

  _createClass(Menu, [{
    key: "emptyFn",
    value: function emptyFn(val) {
      console.warn("Method not implemented in menu");
      console.info("Val: ".concat(val));
    }
  }, {
    key: "render",
    value: function render() {
      var _this6 = this;

      console.log(this.cb);
      return (0, _react2.createElement)("div", null, (0, _react2.createElement)("a", {
        id: "MenuBtn",
        onClick: function onClick() {
          return _this6.setState({
            isMenuOpened: true
          });
        },
        css: {
          backgroundColor: redColor,
          width: 40,
          height: 40,
          position: "absolute",
          top: 20,
          left: 20,
          borderRadius: 20,
          boxShadow: "2.5px 2.5px 10px 1px ".concat(orangeColor),
          cursor: "pointer"
        }
      }, (0, _react2.createElement)("div", {
        className: "gd-menu-icon"
      }, (0, _react2.createElement)("span", {
        className: "material-icons"
      }, "apps"))), (0, _react2.createElement)(GambiMenu, {
        isOpen: this.state.isMenuOpened,
        menuItems: this.items,
        onCloseButtonClick: function onCloseButtonClick() {
          return _this6.setState({
            isMenuOpened: false
          });
        },
        transitionDuration: .75,
        callback: this.cb
      }), this.props.children);
    }
  }]);

  return Menu;
}(_react.Component);

var _default = Menu;
exports.default = _default;