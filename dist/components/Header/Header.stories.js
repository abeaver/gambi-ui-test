"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.bigHeader = exports.allOptions = exports.basic = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Header = require("./Header");

var _GambiApp = _interopRequireDefault(require("../../page/App/GambiApp"));

var _react2 = require("@storybook/react");

var _addonInfo = require("@storybook/addon-info");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _react2.addDecorator)(_addonInfo.withInfo);
var _default = {
  title: 'Header',
  decorators: [_addonInfo.withInfo]
};
exports.default = _default;

var basic = function basic() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Header.Header, {
    bkg: "https://images.unsplash.com/photo-1557682260-96773eb01377"
  }, /*#__PURE__*/_react.default.createElement(_Header.HeaderTitle, null, "Title")));
};

exports.basic = basic;

var allOptions = function allOptions() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Header.Header, {
    bkg: "https://images.unsplash.com/photo-1579548122080-c35fd6820ecb",
    darkBkg: true
  }, /*#__PURE__*/_react.default.createElement(_Header.HeaderColumn, null, /*#__PURE__*/_react.default.createElement(_Header.HeaderPretitle, null, "Pre-title"), /*#__PURE__*/_react.default.createElement(_Header.HeaderTitle, null, "Title")), /*#__PURE__*/_react.default.createElement(_Header.HeaderColumn, null, /*#__PURE__*/_react.default.createElement(_Header.HeaderStatGroup, null, /*#__PURE__*/_react.default.createElement(_Header.HeaderStat, {
    val: "123",
    sub: "Sub"
  }), /*#__PURE__*/_react.default.createElement(_Header.HeaderStat, {
    val: "123",
    sub: "Sub"
  })))));
};

exports.allOptions = allOptions;

var bigHeader = function bigHeader() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Header.BigHeader, {
    bkg: "https://images.unsplash.com/photo-1579548122080-c35fd6820ecb",
    darkBkg: true
  }, /*#__PURE__*/_react.default.createElement(_Header.HeaderTitle, null, "Hello"), /*#__PURE__*/_react.default.createElement(_Header.BigHeaderSubtitle, null, "There")));
};

exports.bigHeader = bigHeader;