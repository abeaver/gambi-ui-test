"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.slider = exports.multiSelectSpinner = exports.multiColumnSpinner = exports.singleColumnSpinner = exports.input = exports.default = void 0;

var _GambiApp = _interopRequireDefault(require("../../page/App/GambiApp"));

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@storybook/react");

var _addonInfo = require("@storybook/addon-info");

var _addonConsole = require("@storybook/addon-console");

var _Input = require("./Input");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _addonConsole.setConsoleOptions)({
  panelExclude: []
});
(0, _react2.addDecorator)(_addonInfo.withInfo); // Place outside of router

var _default = {
  title: 'Inputs',
  decorators: [_addonInfo.withInfo]
};
exports.default = _default;

var input = function input() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Input.Input, {
    placeholder: "Enter text"
  }), /*#__PURE__*/_react.default.createElement("br", null), /*#__PURE__*/_react.default.createElement("br", null), /*#__PURE__*/_react.default.createElement(_Input.Input, {
    black: true
  }));
};

exports.input = input;

var singleColumnSpinner = function singleColumnSpinner() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Input.Spinner, {
    cols: [["Q1", "Q2", "Q3", "Q4"]],
    onUpdate: function onUpdate(val) {
      console.log(val);
    }
  }));
};

exports.singleColumnSpinner = singleColumnSpinner;

var multiColumnSpinner = function multiColumnSpinner() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Input.Spinner, {
    cols: [["Q1", "Q2", "Q3", "Q4"], ["2020", "2019", "2018", "2017"]],
    onUpdate: function onUpdate(val) {
      console.log(val);
    }
  }));
};

exports.multiColumnSpinner = multiColumnSpinner;

var multiSelectSpinner = function multiSelectSpinner() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Input.Spinner, {
    cols: [["Q1", "Q2", "Q3", "Q4"], ["2020", "2019", "2018", "2017"]],
    multi: [0, 1],
    onUpdate: function onUpdate(val) {
      console.log(val);
    }
  }));
};

exports.multiSelectSpinner = multiSelectSpinner;

var slider = function slider() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Input.Slider, {
    min: 0,
    max: 1000,
    onUpdate: function onUpdate(value) {
      console.clear();
      console.log(value);
    }
  }), /*#__PURE__*/_react.default.createElement("p", null, "Open the console to view the output data"));
};

exports.slider = slider;