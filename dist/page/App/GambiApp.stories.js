"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.withBody = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _GambiApp = _interopRequireDefault(require("./GambiApp"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = {
  title: 'Gambi App'
};
exports.default = _default;

var withBody = function withBody() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement("h1", null, "This is a Gambi App"));
};

exports.withBody = withBody;