import React, { Component } from "react";
import "./GambiApp.css"
export default class GambiApp extends Component{
    render(){
        return(
            <div className="gd-app">
                <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet" />
                <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
                      rel="stylesheet" />

                {this.props.children}
            </div>
        )
    }
}