import React, {Component} from "react";
import "./Input.css"
import BottomDrawer from "../BottomDrawer/BottomDrawer";
import Button from "../Button/Button";
import List from "../List/List";


class Input extends Component {


    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.placeholder != null || this.props.placeholder == "") {
            this.placeholder = ""
        } else {
            this.placeholder = this.props.placeholder;
        }
        if (this.props.black) {
            this.theme = "gd-color-black";
        } else {
            this.theme = "gd-color-primary";

        }
        return (
            <input type={"text"} className={`gd-text-input ${this.theme}`} placeholder={this.props.placeholder}/>
        )
    }
}

class Spinner extends Component {
    constructor(props) {
        super(props);
        this.state = {open: false, val: []};
        this.onFinish = this.closeSpinner.bind(this);
        this.onStart = this.openSpinner.bind(this);
        this.cols = this.props.cols;

        this.cb = this.updateVal.bind(this);

        if(this.props.multi){
            this.mi = this.props.multi;
        }else{
            this.mi = [];
        }

        if(this.props.onUpdate){
            this.callback = this.props.onUpdate.bind(this);
        }else{
            this.callback = this.emptyFun.bind(this);
        }
    }

    emptyFun(){
        console.warn("Update Method not Implemented")
    }

    closeSpinner() {

        this.setState({
            open: false
        })
    }

    openSpinner() {
        this.setState({
            open: true
        })
    }

    updateVal(index, val){
        var aval = this.state.val;
        aval[index] = val;
        this.state.val = aval;

        this.callback(aval);


    }



    render() {
        return (
            <div>
                <Button black outline onClick={this.onStart}>Open</Button>
                <BottomDrawer open={this.state.open} onFinished={this.onFinish}>
                    <div className={"gd-spinner-container"}>

                        {this.props.cols.map((col, index) => {
                            return(
                            <div className={"gd-spinner-col"} key={index}>
                                <List options={col} selectable multi={this.mi.includes(index)} onUpdate={(val) => {this.cb(index,val);}}/>

                            </div>);

                        })}
                    </div>

                </BottomDrawer>
            </div>

        )
    }
}

class Slider extends Component {

    constructor(props) {
        super(props);
        this.state = {value: (this.props.max + this.props.min) / 2};

        this.onChange = this.onChange.bind(this)

        this.listen = props.onUpdate.bind(this)

    }

    onChange(event) {
        this.listen(event.target.value)
        this.setState({
            value: event.target.value
        });
    }

    render() {
        return (
            <div className="slidecontainer">
                <input type="range" min={this.props.min} max={this.props.max} value={this.state.value}
                       className={"slider"} onChange={this.onChange}/>
            </div>
        );
    }
}

export {
    Input,
    Slider,
    Spinner
}