import React, { Component } from "react";
import "./Text.css";
class H1 extends Component{
    render() {
        return(
            <h1 className="gd-h1">{this.props.children}</h1>
        )
    }
}

class H2 extends Component{
    render() {
        return(
            <h2 className="gd-h2">{this.props.children}</h2>
        )
    }
}

class H3 extends Component{
    render() {
        return(
            <h3 className="gd-h3">{this.props.children}</h3>
        )
    }
}
class H4 extends Component{
    render() {
        return(
            <h4 className="gd-h4">{this.props.children}</h4>
        )
    }
}

class P extends Component{
    render() {
        return(
            <p className="gd-paragraph">{this.props.children}</p>
        )
    }
}

export {
    H1,
    H2,
    H3,
    H4,
    P
}