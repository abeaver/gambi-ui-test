import GambiApp from "../../page/App/GambiApp";
import React from "react";

import { addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import { setConsoleOptions } from '@storybook/addon-console';
import Button from "./Button";

setConsoleOptions({
    panelExclude: [],
});

addDecorator(withInfo)
// Place outside of router

export default { title: 'Button', decorators: [withInfo] };
export const basic = () => <GambiApp>
    <Button>Click Me</Button>
</GambiApp>

export const small = () => <GambiApp>
    <Button small>Click Me</Button>
</GambiApp>

export const large = () => <GambiApp>
    <Button large>Click Me</Button>
</GambiApp>

export const full = () => <GambiApp>
    <Button full>Click Me</Button>
</GambiApp>


export const outline = () => <GambiApp>
    <Button outline>Click Me</Button>
</GambiApp>

export const rounded = () => <GambiApp>
    <Button rounded>Click Me</Button>
</GambiApp>

export const pill = () => <GambiApp>
    <Button pill>Click Me</Button>
</GambiApp>

export const largeRoundedOutline = () => <GambiApp>
    <Button large rounded outline>Click Me</Button>
</GambiApp>

export const withCallback = () => <GambiApp>
    <Button onClick={()=>{console.log("Button Clicked")}}>Click Me</Button>
    <p>Open the console to view the output data</p>

</GambiApp>

export const black = () => <GambiApp>
    <Button black>Click Me</Button>
</GambiApp>

export const blackOutline = () => <GambiApp>
    <Button black outline>Click Me</Button>
</GambiApp>