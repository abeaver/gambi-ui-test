import React, {Component} from "react";
import './List.css';
class ListItem extends Component{
    constructor(props) {
        super(props);
        this.onClick = this.props.onClick.bind(this);
    }
    render() {
        return(
            <div className={`gd-list-item ${this.props.active?"gd-list-item-selected":""}`} onClick={()=>this.onClick(this.props.title)}>
            {this.props.active? <span className="material-icons">done</span> : ""}
            <span className={"gd-list-item-title"}>{this.props.title}</span>
        </div>
        );
    }
}
export default class List extends Component {




    state = {
        multiSelect: false,
        updates: 0,
        selected: []
    }
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this)

        if(this.props.onUpdate != null){
            this.onUpdate = this.props.onUpdate.bind(this);
        }else{
            this.onUpdate = this.blankFn.bind(this);
        }
    }
    filterRemove(list, val) {
        var filteredItems = list.filter(item => item !== val)
        return filteredItems;
    }

    onClick(value){
        if(this.props.selectable){
            if(this.props.multi){
                if(this.state.selected.includes(value)){
                    this.setState({
                        selected: this.filterRemove(this.state.selected, value),
                        updates: this.state.updates + 1
                    })
                }else{
                    this.state.selected.push(value)
                    this.setState({
                        updates: this.state.updates + 1
                    })
                }
            }else{
                if(this.state.selected.includes(value)){

                    this.setState({
                        selected: []
                    })
                }else{
                    this.setState({
                        selected: value
                    });
                }

            }


        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.onUpdate(this.state.selected);

    }

    blankFn(){
        console.warn("List on click method not implemented")
    }
    render() {

        return (
            <div className={"gd-list"}>
                {this.props.options.map((option) => {
                    return(<ListItem key={option} active={this.state.selected.includes(option)} onClick={this.onClick} title={option} />);

                })}
            </div>
        );
    }
}
export {
    ListItem
}