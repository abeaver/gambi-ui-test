import React, { Component } from "react";
import "./Header.css"

class HeaderColumn extends Component{
    render() {
        return (
            <div className="gd-header-column">
                {this.props.children}
            </div>
        );
    }
}

class HeaderPretitle extends Component{
    render() {
        return(
            <div className="gd-header-pretitle">
                {this.props.children}
            </div>
        )
    }
}

class HeaderTitle extends Component{
    render() {
        return(
            <div className="gd-header-title">
                {this.props.children}
            </div>
        )
    }
}

class HeaderStatGroup extends Component{
    render() {
        return( <div className="gd-header-stat-box">
                {this.props.children}
            </div>
        )
    }
}

class HeaderStat extends Component{
    render() {
        return(<div className="gd-header-stat">
            <div className="gd-header-stat-subtitle">
                {this.props.sub}
            </div>
            <div className="gd-header-stat-value">
                {this.props.val}
            </div>

        </div>)
    }
}

class Header extends Component{
    render(){
        var styles = {backgroundImage: "url("+this.props.bkg+")"};


        return(
            <div className={`gd-header ${this.props.darkBkg ? 'gd-header-dark' : 'gd-header-light'}`} style={styles}>

                {this.props.children}
            </div>
        )
    }
}

class BigHeader extends Component{
    render() {
        var styles = {backgroundImage: "url("+this.props.bkg+")"};
        return (
            <div className={`gd-big-header ${this.props.darkBkg ? 'gd-header-dark' : 'gd-header-light'}`} style={styles}>
                <div className={"gd-big-header-content"}>
                    {this.props.children}

                </div>
            </div>
        );
    }
}

class BigHeaderSubtitle extends Component{
    render() {
        return(
            <div className={"gd-header-subtitle"}>
                {this.props.children}
            </div>
        )
    }
}

export {
    Header,
    HeaderColumn,
    HeaderPretitle,
    HeaderTitle,

    HeaderStatGroup,
    HeaderStat,

    BigHeader,
    BigHeaderSubtitle
}